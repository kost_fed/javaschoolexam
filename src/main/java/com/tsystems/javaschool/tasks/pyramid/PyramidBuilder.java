package com.tsystems.javaschool.tasks.pyramid;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PyramidBuilder {
    public int[][] buildPyramid(List<Integer> inputNumbers) throws CannotBuildPyramidException{
        //check if pyramid is buildable
        if (inputNumbers == null || inputNumbers.contains(null)){
            throw new CannotBuildPyramidException();
        }
        boolean isPyramidable = false;
        int rowCounter = 0;
        int sum = 0;

        while (sum <= inputNumbers.size()){
            //check for integer overflow
            if (sum > sum + rowCounter){
                throw new CannotBuildPyramidException();
            }
            sum += ++rowCounter;
            if (sum == inputNumbers.size()){
                isPyramidable = true;
                break;
            }
        }
        if (!isPyramidable) {
            throw new CannotBuildPyramidException();
        }
        //Start building the pyramid
        int[][] result = new int[rowCounter][rowCounter*2 - 1];
        for (int[] row : result) {
            Arrays.fill(row, 0);
        }
        //Create a copy to preserve the original List, since our method just builds a pyramid and shouldn't modify the argument
        ArrayList<Integer> tempArray = new ArrayList<>(inputNumbers);
        tempArray.sort(null);

        int currentIndex = 0;
        for (int row = 0; row < rowCounter; ++row){
            int pasteIndex = rowCounter - row - 1;
            for (int rowNumberAmount = 0; rowNumberAmount < row + 1; ++rowNumberAmount){
                result[row][pasteIndex] = tempArray.get(currentIndex++);
                pasteIndex += 2;
            }
        }
        return result;
    }
}
