package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) throws IllegalArgumentException {
        if (x == null || y == null) throw new IllegalArgumentException();
        int currentIndex = 0;
        int counter = 0;
        boolean result = false;

        for (int targetCounter = 0; targetCounter < x.size(); ++targetCounter){
            while (currentIndex < y.size()){
                if (y.get(currentIndex++) == x.get(targetCounter)){
                    counter++;
                    break;
                }
            }
            if (counter != targetCounter + 1){
                break;
            }
        }
        //We have a counter that increases with each match.
        //If by the end of Y list there were less matches than members in X
        //Then it means you can't fulfill the criteria (can't get X list by deleting elements from Y list)
        if (counter == x.size()){
            result = true;
        }
        return result;
    }
}