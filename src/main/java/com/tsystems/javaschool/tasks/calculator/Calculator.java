package com.tsystems.javaschool.tasks.calculator;

import java.text.DecimalFormat;
import java.util.ArrayDeque;

public class Calculator {

    public String evaluate(String statement) {
        if (statement == null || statement.equals("")) {
            return null;
        }
        String statementNew = getPostfixString(statement);
        if (statementNew == null) {
            return null;
        }
        ArrayDeque<Double> stack = new ArrayDeque<>();
        String[] splitStatement = statementNew.split(" ");
        String current;
        double operandA, operandB;

        for (int i = 0; i < splitStatement.length; i++) {
            try {
                current = splitStatement[i];
                if (isOperator(current.charAt(0))) {
                    if (stack.size() < 2) {
                        return null;
                    }
                    operandB = stack.pop();
                    operandA = stack.pop();
                    if (current.charAt(0) == '+') {
                        stack.push(operandA + operandB);
                    } else if (current.charAt(0) == '-') {
                        stack.push(operandA - operandB);
                    } else if (current.charAt(0) == '*') {
                        stack.push(operandA * operandB);
                    } else {
                        stack.push(operandA / operandB);
                    }
                } else {
                    operandA = Double.parseDouble(current);
                    stack.push(operandA);
                }
                // This is where all kinds of faulty inputs are caught: letters, double operators, etc
            } catch (NumberFormatException | StringIndexOutOfBoundsException e) {
                return null;
            }
        }

        Double finalResult = stack.pop();
        DecimalFormat df = new DecimalFormat("#.####");
        if (Double.isFinite(finalResult)) {
            return df.format(finalResult);
        } else {
            return null;
        }
    }

    public static boolean isOperator(char inputChar) {
        return inputChar == '+' || inputChar == '-' || inputChar == '*' || inputChar == '/';
    }

    public static char getOperatorPriority(char operator) {
        if (operator == '+' || operator == '-'){
            return 1;
        }
        return 2;
    }

    public static String getPostfixString(String inputString) {
        char currentChar;
        char stackTop;
        StringBuilder result = new StringBuilder("");
        StringBuilder stack = new StringBuilder("");
        for (int i = 0; i < inputString.length(); i++) {
            currentChar = inputString.charAt(i);
            if (isOperator(currentChar)) {
                while (stack.length() > 0) {
                    stackTop = stack.charAt(stack.length() - 1);
                    if (isOperator(stackTop) && (getOperatorPriority(currentChar) <= getOperatorPriority(stackTop))) {
                        result.append(" ");
                        result.append(stackTop);
                        stack.setLength(stack.length() - 1);
                    } else {
                        break;
                    }
                }
                result.append(" ");
                stack.append(currentChar);
            } else if ('(' == currentChar) {
                stack.append(currentChar);
            } else if (')' == currentChar) {
                try {
                    //dump all stack from the top to an opening parenthesis into result
                    stackTop = stack.charAt(stack.length() - 1);
                    while (stackTop != '(') {
                        if (stack.length() <= 1) {
                            return null;
                        }
                        result.append(" ").append(stackTop);
                        stack.setLength(stack.length() - 1);
                        stackTop = stack.charAt(stack.length() - 1);
                    }
                    stack.setLength(stack.length() - 1);
                } catch (IndexOutOfBoundsException e) {
                    return null;
                }
            } else {
                result.append(currentChar);
            }
        }

        while (stack.length() > 0) {
            result.append(" ").append(stack.charAt(stack.length() - 1));
            stack.setLength(stack.length() - 1);
        }
        return result.toString();
    }
}
